﻿using LearnMvc.DataAccess;
using LearnMvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LearnMvc.LearnMvcBLL
{
    public class EmployeeBLL
    {
        public List<Employee> GetEmployees()
        {
            using (var context = new SalesERPDAL())
            {
                return context.Employees.ToList();
            }
        }

        public Employee SaveEmployee(Employee e)
        {
            using (var context = new SalesERPDAL())
            {
                context.Employees.Add(e);
                context.SaveChanges();
                return e;
            }
        }

        public bool IsValidUser(UserDetails u)
        {
            if (u.UserName == "Admin" && u.Password == "Admin")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}